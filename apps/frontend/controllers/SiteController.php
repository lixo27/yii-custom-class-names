<?php

namespace app\controllers;

use customClassNames\interfaces\generators\IndexGeneratorAction;
use yii\web\Controller;

/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => IndexGeneratorAction::class,
        ];
    }
}
