<?php /** @var \yii\web\View $this */ ?>
<?php /** @var string[] $customClassNames */ ?>

<?php $this->title = 'Custom Class Names!'; ?>

<div class="jumbotron">

    <h1>Custom Class Names!</h1>

    <p class="lead">Mauris pharetra eros accumsan risus lacinia pretium a eget nulla. Phasellus non vulputate neque.</p>

</div><!-- .jumbotron -->

<div class="row">

    <div class="col-lg-6 col-lg-push-3">

        <table class="table table-bordered">

            <tbody>

            <?php foreach ( $customClassNames as $customClassName ): ?>

                <tr>
                    <td>
                        <code>
                            <?= $customClassName; ?>
                        </code>
                    </td>
                </tr>

            <?php endforeach; ?>

            </tbody>

        </table><!-- .table -->

    </div><!-- .col-lg-12 -->

</div><!-- .row -->
