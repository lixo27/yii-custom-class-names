<?php

return call_user_func( function () {
    return [
        'id' => 'frontend',
        'basePath' => dirname( __DIR__ ),
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm' => '@vendor/npm-asset',
        ],
        'components' => [
            'request' => [
                'cookieValidationKey' => 'bUr3et8nut2cyPPH8TgcER9t4t9ZkxnJ',
            ],
        ],
    ];
} );
