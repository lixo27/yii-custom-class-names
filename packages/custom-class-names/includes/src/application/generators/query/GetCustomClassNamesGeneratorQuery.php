<?php

namespace customClassNames\application\generators\query;

use customClassNames\domain\generators\UniqueIdGeneratorFactory;

/**
 * Class GetCustomClassNamesGeneratorQuery
 *
 * @package customClassNames\application\generators\query
 */
class GetCustomClassNamesGeneratorQuery
{
    /**
     * @var UniqueIdGeneratorFactory
     */
    private UniqueIdGeneratorFactory $uniqueIdGeneratorFactory;

    /**
     * GetCustomClassNamesGeneratorQuery constructor.
     *
     * @param UniqueIdGeneratorFactory $uniqueIdGeneratorFactory
     */
    public function __construct( UniqueIdGeneratorFactory $uniqueIdGeneratorFactory )
    {
        $this->uniqueIdGeneratorFactory = $uniqueIdGeneratorFactory;
    }

    public function execute(): array
    {
        $uniqueId = $this->uniqueIdGeneratorFactory->create();

        return [
            sprintf( 'is-custom-styled-%s', $uniqueId ),
            sprintf( 'is-custom-styled is-custom-styled-%s', $uniqueId ),
            sprintf( 'has-custom-styles-%s', $uniqueId ),
            sprintf( 'has-custom-styles has-custom-styles-%s', $uniqueId ),
        ];
    }
}
