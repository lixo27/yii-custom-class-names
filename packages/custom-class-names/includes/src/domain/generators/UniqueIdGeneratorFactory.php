<?php

namespace customClassNames\domain\generators;

/**
 * Class UniqueIdGeneratorFactory
 *
 * @package customClassNames\domain\generators
 */
class UniqueIdGeneratorFactory
{
    public function create(): string
    {
        $uniqueId = md5( uniqid( rand(), true ) );

        return substr( $uniqueId, 0, 8 );
    }
}
