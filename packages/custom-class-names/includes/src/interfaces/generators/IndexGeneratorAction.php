<?php

namespace customClassNames\interfaces\generators;

use customClassNames\application\generators\query\GetCustomClassNamesGeneratorQuery;
use yii\base\Action;

/**
 * Class IndexGeneratorAction
 *
 * @package customClassNames\interfaces\generators
 */
class IndexGeneratorAction extends Action
{
    /**
     * @var GetCustomClassNamesGeneratorQuery
     */
    private GetCustomClassNamesGeneratorQuery $customClassNamesGeneratorQuery;

    /**
     * IndexGeneratorAction constructor.
     *
     * @param                                   $id
     * @param                                   $controller
     * @param GetCustomClassNamesGeneratorQuery $customClassNamesGeneratorQuery
     * @param array                             $config
     */
    public function __construct(
        $id,
        $controller,
        GetCustomClassNamesGeneratorQuery $customClassNamesGeneratorQuery,
        $config = []
    )
    {
        parent::__construct( $id, $controller, $config );
        $this->customClassNamesGeneratorQuery = $customClassNamesGeneratorQuery;
    }

    public function run()
    {
        return $this->controller->render( 'index', [
            'customClassNames' => $this->customClassNamesGeneratorQuery->execute(),
        ] );
    }
}
